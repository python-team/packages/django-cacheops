Source: django-cacheops
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Carsten Schoenert <c.schoenert@t-online.de>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
Build-Depends-Indep:
 python3-django,
 python3-funcy,
 python3-redis,
 python3-setuptools,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-cacheops
Vcs-Git: https://salsa.debian.org/python-team/packages/django-cacheops.git
Homepage: https://github.com/Suor/django-cacheops

Package: python3-django-cacheops
Architecture: all
Depends:
 python3-django,
 ${misc:Depends},
 ${python3:Depends},
Description: Django app adding automatic or manual queryset caching (Python3)
 django-cacheops is a slick app that supports automatic or manual queryset
 caching and automatic granular event-driven invalidation.
 .
 It uses redis as backend for ORM cache and redis or filesystem for simple
 time-invalidated one.
 .
 And there is more to it:
 .
  * decorators to cache any user function or view as a queryset or by time
  * extensions for django and jinja2 templates
  * transparent transaction support
  * dog-pile prevention mechanism
  * a couple of hacks to make django faster
 .
 This package contains the Python 3 version of the library.
